#-------------------------------------------------
#
# Project created by QtCreator 2014-04-24T15:15:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageScrambler
TEMPLATE = app


SOURCES += main.cpp \
    ImageScrambler.cpp \
    Filter.cpp

HEADERS  += \
    ImageScrambler.h \
    ImageScrambler.h

FORMS    += \
    ImageScrambler.ui

RESOURCES += \
    images.qrc \
    style.qrc
